# wl Spring Assistant

#### 介绍
This is a Spring plugin developed based on the IntelliJ IDEA Community Edition, which mainly supports the parsing of annotations @ConfigurationProperties and @Value, and provides functions such as auto-completion and code navigation. It supports file types including application-*.properties and application-*.yaml(yml) files.

这是一款基于 idea 社区版所开发的 Spring 插件，主要支持 @ConfigurationProperties 和 @Value 注解的解析，并提供自动补全、代码跳转等功能，支持的文件类型包括 application-*.properties 和 application-*.yaml(yml) 文件


#### 安装教程
file -> setting -> plugins
搜索wl spring assistant，点击install

#### 使用说明
![输入图片说明](IMG/ConfigurationPropertiesAutoCompletion.gif)

<video width="640" height="360" controls>
  <source src="IMG/configurationPropertiesAutoCompletion.mp4" type="video/mp4">
</video>
