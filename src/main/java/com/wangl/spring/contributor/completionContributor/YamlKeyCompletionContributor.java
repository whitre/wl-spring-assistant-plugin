package com.wangl.spring.contributor.completionContributor;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.patterns.PlatformPatterns;
import com.wangl.spring.suggestion.filetype.SpringConfigurationYamlFileType;
import org.jetbrains.yaml.YAMLLanguage;

import static com.intellij.patterns.PsiJavaPatterns.virtualFile;

/**
 * @ClassName YamlKeyCompletionContributor
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/28 16:30
 */
public class YamlKeyCompletionContributor extends CompletionContributor {
    public YamlKeyCompletionContributor() {
        extend(CompletionType.BASIC,
                PlatformPatterns.psiElement().withLanguage(YAMLLanguage.INSTANCE)
                .inVirtualFile(virtualFile().ofType(SpringConfigurationYamlFileType.INSTANCE)),
                new YamlCompletionProvider());
    }
}
