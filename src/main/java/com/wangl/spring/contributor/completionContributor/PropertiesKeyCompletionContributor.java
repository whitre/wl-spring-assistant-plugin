package com.wangl.spring.contributor.completionContributor;

import com.intellij.codeInsight.completion.*;
import com.intellij.lang.properties.parsing.PropertiesTokenTypes;
import com.intellij.patterns.PlatformPatterns;
import com.wangl.spring.suggestion.filetype.SpringConfigurationPropertiesFileType;

import static com.intellij.patterns.PlatformPatterns.virtualFile;

/**
 * @ClassName PropertiesKeyCompletionContributor
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/23 18:28
 */
public class PropertiesKeyCompletionContributor extends CompletionContributor {
    public PropertiesKeyCompletionContributor() {
        extend(CompletionType.BASIC,
                PlatformPatterns.psiElement(PropertiesTokenTypes.KEY_CHARACTERS)
                .inVirtualFile(virtualFile().ofType(SpringConfigurationPropertiesFileType.INSTANCE)),
                new PropertyCompletionProvider());
    }
}
