package com.wangl.spring.suggestion;

import com.intellij.lang.properties.IProperty;
import com.intellij.lang.properties.psi.PropertiesFile;
import com.intellij.psi.PsiElement;
import com.intellij.openapi.module.Module;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @ClassName PropertiesManager
 * @Description TODO
 * @Author wangl
 * @Date 2023/4/27 15:43
 */
public class PropertiesManager {

    private final Set<PropertiesFile> projectPropertiesFiles = new CopyOnWriteArraySet<>();

    private final Map<Module, Set<PropertiesFile>> moduleGroupMap = new ConcurrentHashMap<>();

    public void insert(PropertiesFile propertiesFile, Module module){
        projectPropertiesFiles.add(propertiesFile);
        moduleGroupMap.computeIfAbsent(module, k -> new CopyOnWriteArraySet<>()).add(propertiesFile);
    }

    public Set<PropertiesFile> search(Module module){
        if (module == null){
            return projectPropertiesFiles;
        }else {
            return moduleGroupMap.getOrDefault(module, Collections.emptySet());
        }
    }

    public void remove(PropertiesFile propertiesFile, Module module) {
        projectPropertiesFiles.remove(propertiesFile);
        if (module != null){
            Set<PropertiesFile> modulePropertiesFiles = moduleGroupMap.get(module);
            if (CollectionUtils.isNotEmpty(modulePropertiesFiles)){
                modulePropertiesFiles.remove(propertiesFile);
            }
        }
    }
}
