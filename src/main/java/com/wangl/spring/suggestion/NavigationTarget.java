package com.wangl.spring.suggestion;

import com.intellij.psi.PsiElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName NavigationTarget
 * @Description TODO
 * @Author wangl
 * @Date 2023/4/26 9:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavigationTarget {

    private PsiElement psiElement;
}
