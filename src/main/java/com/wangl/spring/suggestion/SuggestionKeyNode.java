package com.wangl.spring.suggestion;

import com.intellij.psi.PsiElement;
import lombok.*;

import java.util.*;

/**
 * @ClassName SuggestionKeyNode
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/29 17:32
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "text")
public class SuggestionKeyNode {

    private String text;
    private SuggestionKeyNode parent;
    private List<SuggestionKeyNode> children;
    private SuggestionNodeType type;
    private int layerLevel;

    public static SuggestionKeyNode newInstance(String text, SuggestionKeyNode parent){
        return SuggestionKeyNode.builder()
                .text(text)
                .parent(parent)
                .children(new ArrayList<>())
                .build();
    }

    public void addChild(SuggestionKeyNode child){
        this.getChildren().add(child);
    }

    public String getSuggestion(){
        if (this.getParent() != null && this.getParent().getLayerLevel() > 0){
            return this.getParent().getSuggestion() + "." + this.getText();
        }
        return this.getText();
    }
}
