package com.wangl.spring.service.chain;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.wangl.spring.suggestion.SuggestionNodeTree;

public interface SuggestionTreeBuilder {

    void build(SuggestionNodeTree suggestionNodeTree, PsiClass psiClass);

    void build(SuggestionNodeTree suggestionNodeTree, PsiMethod psiMethod);

    void build(SuggestionNodeTree suggestionNodeTree, PsiField psiField);
}
