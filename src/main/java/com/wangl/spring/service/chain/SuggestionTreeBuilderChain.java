package com.wangl.spring.service.chain;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.wangl.spring.suggestion.SuggestionNodeTree;

import java.util.ArrayList;
import java.util.List;

public class SuggestionTreeBuilderChain implements SuggestionTreeBuilder {

    private static final SuggestionTreeBuilderChain instance = new SuggestionTreeBuilderChain();

    public static SuggestionTreeBuilderChain getInstance(){return instance;}

    private final List<SuggestionTreeBuilder> suggestionTreeBuilderList;

    public SuggestionTreeBuilderChain(){
        this.suggestionTreeBuilderList = new ArrayList<>();
        suggestionTreeBuilderList.add(new ConfigurationPropertiesSuggestionTreeBuilder());
        suggestionTreeBuilderList.add(new ValueAnnotationSuggestionTreeBuilder());
        suggestionTreeBuilderList.add(new ConditionOnPropertiesSuggestionTreeBuilder());
    }

    @Override
    public void build(SuggestionNodeTree suggestionNodeTree, PsiClass psiClass) {
        for (SuggestionTreeBuilder suggestionTreeBuilder : suggestionTreeBuilderList) {
            suggestionTreeBuilder.build(suggestionNodeTree, psiClass);
        }
    }

    @Override
    public void build(SuggestionNodeTree suggestionNodeTree, PsiMethod psiMethod) {
        for (SuggestionTreeBuilder suggestionTreeBuilder : suggestionTreeBuilderList) {
            suggestionTreeBuilder.build(suggestionNodeTree, psiMethod);
        }
    }

    @Override
    public void build(SuggestionNodeTree suggestionNodeTree, PsiField psiField) {
        for (SuggestionTreeBuilder suggestionTreeBuilder : suggestionTreeBuilderList) {
            suggestionTreeBuilder.build(suggestionNodeTree, psiField);
        }
    }
}
