package com.wangl.spring.declarationHandler;

import com.intellij.codeInsight.navigation.actions.GotoDeclarationHandler;
import com.intellij.lang.properties.psi.impl.PropertyKeyImpl;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.wangl.spring.service.SuggestionService;
import com.wangl.spring.utils.PsiCustomUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @ClassName PropertiesKeyGotoDeclarationHandler
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/27 15:01
 */
public class PropertiesKeyGotoDeclarationHandler implements GotoDeclarationHandler {

    @Override
    public PsiElement @Nullable [] getGotoDeclarationTargets(@Nullable PsiElement source, int offset, Editor editor) {
        if (!(source instanceof PropertyKeyImpl)) return null;
        Module module = PsiCustomUtil.findModule(source);
        SuggestionService service = module.getService(SuggestionService.class);
        if (!service.isCanSuggest()){
            return null;
        }
        Document document = editor.getDocument();
        int lineNumber = document.getLineNumber(offset);
        int lineStartOffset = document.getLineStartOffset(lineNumber);
        String selectionText = document.getText(new TextRange(lineStartOffset, offset));
        String text = source.getText();

        String prefix = getPrefix(selectionText,text).replaceAll("\\[\\d*\\]", "");
        Set<PsiElement> psiElements = service.searchByPropertyName(prefix);
        if (CollectionUtils.isEmpty(psiElements)){
            psiElements = getDefaultTargets(service, text);
            return psiElements.toArray(new PsiElement[0]);
        }
        return psiElements.toArray(new PsiElement[0]);
    }

    private String getPrefix(String selectionText, String text){
        String[] selectionGroups = selectionText.split("\\.");
        String[] textGroups = text.split("\\.");
        if (selectionGroups.length >= textGroups.length){
            return text;
        }
        List<String> groups = new ArrayList<>();
        for (int i = 0; i < selectionGroups.length; i++) {
            groups.add(textGroups[i]);
        }
        return String.join(".", groups);
    }

    private Set<PsiElement> getDefaultTargets(SuggestionService service, String text){
        String[] textGroups = text.split("\\.");
        List<String> groups = new ArrayList<>(Arrays.asList(textGroups));
        int i = textGroups.length - 1;
        while (i >= 0){
            String prefix = String.join(".", groups);
            Set<PsiElement> psiElements = service.searchByPropertyName(prefix);
            if (!CollectionUtils.isEmpty(psiElements)){
                return psiElements;
            }
            groups.remove(i);
            i--;
        }
        return Collections.emptySet();
    }
}
