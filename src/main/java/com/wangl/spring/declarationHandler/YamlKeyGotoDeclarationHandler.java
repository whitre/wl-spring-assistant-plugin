package com.wangl.spring.declarationHandler;

import com.intellij.codeInsight.navigation.actions.GotoDeclarationHandler;
import com.intellij.codeInsight.navigation.actions.GotoDeclarationHandlerBase;
import com.intellij.lang.properties.psi.impl.PropertyKeyImpl;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.tree.LeafPsiElement;
import com.wangl.spring.service.SuggestionService;
import com.wangl.spring.utils.PsiCustomUtil;
import com.wangl.spring.utils.StringPropertiesUtil;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.yaml.psi.YAMLKeyValue;
import org.jetbrains.yaml.psi.YAMLScalarText;
import org.jetbrains.yaml.psi.YAMLSequence;
import org.jetbrains.yaml.psi.impl.YAMLFileImpl;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.wangl.spring.utils.PsiCustomUtil.findModule;
import static com.wangl.spring.utils.PsiCustomUtil.truncateIdeaDummyIdentifier;
import static java.util.Objects.requireNonNull;

/**
 * @ClassName YamlKeyGotoDeclarationHandler
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/27 15:01
 */
public class YamlKeyGotoDeclarationHandler implements GotoDeclarationHandler {
    @Override
    public PsiElement @Nullable [] getGotoDeclarationTargets(@Nullable PsiElement source, int offset, Editor editor) {
        if (!(source instanceof LeafPsiElement) || !(source.getContainingFile() instanceof YAMLFileImpl)) return null;
        Module module = findModule(source);
        if (module == null) {
            return null;
        }
        SuggestionService service = module.getService(SuggestionService.class);
        if (!service.isCanSuggest()){
            return null;
        }
        PsiElement elementContext = source.getContext();
        PsiElement parent = requireNonNull(elementContext).getParent();
        if (parent instanceof YAMLSequence) {
            return null;
        }

        List<String> ancestralKeys = new ArrayList<>();
        PsiElement context = elementContext;
        do {
            if (context instanceof YAMLKeyValue) {
                ancestralKeys.add(0, truncateIdeaDummyIdentifier(((YAMLKeyValue) context).getKeyText()));
            }
            context = requireNonNull(context).getParent();
        } while (context != null);

        String propertyName = "";
        if (!CollectionUtils.isEmpty(ancestralKeys)){
            propertyName += String.join(".", ancestralKeys);
        }
        Set<PsiElement> psiElements = service.searchByPropertyName(propertyName);
        if (CollectionUtils.isEmpty(psiElements)){
            return null;
        }
        return psiElements.toArray(new PsiElement[0]);
    }
}
