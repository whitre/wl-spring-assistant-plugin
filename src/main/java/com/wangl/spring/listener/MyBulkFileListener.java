package com.wangl.spring.listener;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.lang.properties.PropertiesFileType;
import com.intellij.lang.properties.psi.PropertiesFile;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import com.intellij.openapi.vfs.newvfs.events.VFileEvent;
import com.intellij.openapi.vfs.newvfs.events.VFileDeleteEvent;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import com.wangl.spring.service.ProjectSuggestionService;
import com.wangl.spring.service.SuggestionService;
import com.wangl.spring.suggestion.PropertiesManager;
import com.wangl.spring.utils.ProjectUtil;

import java.util.List;
import java.util.Objects;

import static com.wangl.spring.utils.PsiCustomUtil.findFile;
import static com.wangl.spring.utils.PsiCustomUtil.findModule;

public class MyBulkFileListener implements BulkFileListener {

    @Override
    public void before(List<? extends VFileEvent> events) {
        Project currentProject = ProjectUtil.getCurrentProject();
        if (currentProject == null || !currentProject.isOpen()){
            return;
        }
        for (VFileEvent event : events) {
            if (event instanceof VFileDeleteEvent){
                VirtualFile file = event.getFile();
                boolean fileValid = file == null ||
                        !file.isValid() ||
                        (!Objects.equals(file.getExtension(), PropertiesFileType.DEFAULT_EXTENSION) &&
                                !Objects.equals(file.getExtension(), JavaFileType.DEFAULT_EXTENSION));
                if (fileValid){
                    return;
                }

                DumbService.getInstance(currentProject).runWhenSmart(() -> {
                    PsiFile psiFile = findFile(currentProject, file);
                    if (psiFile == null){
                        return;
                    }
                    Module module = findModule(psiFile);
                    if (module == null){
                        return;
                    }
                    SuggestionService service = module.getService(SuggestionService.class);
                    if (!service.isCanSuggest()){
                        return;
                    }
                    if (psiFile instanceof PsiJavaFile && Objects.equals(psiFile.getVirtualFile().getExtension(), JavaFileType.DEFAULT_EXTENSION)){
                        service.getSuggestionNodeTree().remove(psiFile);
                    }
                });
            }
        }
    }
}

