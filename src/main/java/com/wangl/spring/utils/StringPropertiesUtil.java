package com.wangl.spring.utils;

import com.intellij.openapi.util.text.StringUtil;

import java.beans.Introspector;

/**
 * @ClassName StringPropertiesUtil
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/30 13:15
 */
public class StringPropertiesUtil {

    public static String toCamelCase(String propertyName, String delimiter){
        String[] strings = propertyName.split(delimiter);
        StringBuilder sb = new StringBuilder(strings[0]);
        for (int i = 1; i < strings.length; i++) {
            sb.append(StringUtil.capitalize(strings[i]));
        }
        return sb.toString();
    }

    public static String kebabCaseToCamelCase(String propertyName){
        return toCamelCase(propertyName, "-");
    }

    public static String camelCaseToKebabCase(String propertyName){
        StringBuilder result = new StringBuilder(propertyName.length());
        boolean inIndex = false;
        for (int i = 0; i < propertyName.length(); i++) {
            char ch = propertyName.charAt(i);
            if (inIndex) {
                result.append(ch);
                if (ch == ']') {
                    inIndex = false;
                }
            }
            else {
                if (ch == '[') {
                    inIndex = true;
                    result.append(ch);
                }
                else {
                    ch = (ch != '_') ? ch : '-';
                    if (Character.isUpperCase(ch) && result.length() > 0 && result.charAt(result.length() - 1) != '-') {
                        result.append('-');
                    }
                    result.append(Character.toLowerCase(ch));
                }
            }
        }
        return result.toString();
    }

    public static String getPropertyNameByMethod(String methodName){
        return StringUtil.getPropertyName(methodName);
    }
}
