package com.wangl.spring.utils;

/**
 * @ClassName Annotations
 * @Description TODO
 * @Author wangl
 * @Date 2023/3/31 17:45
 */
public class Annotations {

    public static final String CONFIGURATION_PROPERTIES = "org.springframework.boot.context.properties.ConfigurationProperties";

    public static final String CONFIGURATION = "org.springframework.context.annotation.Configuration";

    public static final String BEAN = "org.springframework.context.annotation.Bean";

    public static final String TRANSACTIONAL_EVENT_LISTENER = "org.springframework.transaction.event.TransactionalEventListener";

    public static final String COMPONENT = "org.springframework.stereotype.Component";
    public static final String REPOSITORY = "org.springframework.stereotype.Repository";
    public static final String SERVICE = "org.springframework.stereotype.Service";
    public static final String CONTROLLER = "org.springframework.stereotype.Controller";

    public static final String VALUE = "org.springframework.beans.factory.annotation.Value";

    public static final String CONDITIONAL_ON_PROPERTY = "org.springframework.boot.autoconfigure.condition.ConditionalOnProperty";
}
