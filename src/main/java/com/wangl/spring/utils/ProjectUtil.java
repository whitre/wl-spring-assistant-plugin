package com.wangl.spring.utils;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.IdeFrame;
import com.intellij.openapi.wm.WindowManager;

/**
 * @ClassName ProjectUtil
 * @Description TODO
 * @Author wangl
 * @Date 2023/8/7 15:54
 */
public class ProjectUtil {
    public static Project getCurrentProject() {
        IdeFrame ideFrame = WindowManager.getInstance().getIdeFrame(null);
        if (ideFrame != null) {
            return ideFrame.getProject();
        }
        return null;
    }
}
