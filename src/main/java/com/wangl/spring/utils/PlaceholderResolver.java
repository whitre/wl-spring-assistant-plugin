package com.wangl.spring.utils;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName PlaceholderResolver
 * @Description ${} resolve
 * @Author wangl
 * @Date 2023/4/20 16:32
 */
public class PlaceholderResolver {

    public static final String placeholderPrefix = "${";
    public static final String placeholderSuffix = "}";
    public static final String valueSeparator = ":";
    private static final Pattern  placeholderPattern = Pattern.compile("\\$\\{([^${]+?)\\}");

    public static List<String> getPlaceholders(String value){
        if (!value.contains(placeholderPrefix) || !value.contains(placeholderSuffix)){
            return Collections.emptyList();
        }
        List<String> placeholders = new ArrayList<>();
        Matcher matcher = placeholderPattern.matcher(value);
        while (matcher.find()) {
            String group = matcher.group(1);
            if (group.contains(valueSeparator)){
                group = group.substring(0, group.indexOf(valueSeparator));
            }
            placeholders.add(group);
        }
        return placeholders;
    }
}
